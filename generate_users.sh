#!/bin/bash

# Requirements
# faker-cli
# https://www.npmjs.com/package/faker-cli


# ************************* Generate Users *************************
filename="users.json";
touch $filename

echo "generating users"
data="";
data="{";

# Generate first 57 records
for x in `seq 1  56`; 
do 
  echo -n ".";
  data+=",\"user$x\":"; 
  data+=$(faker-cli --helpers userCard);
done;

# Generate flag record
echo -n ".";
data+=",\"user57\":";
data+="{\"name\":\"Aubree Blanda\",\"username\":\"Irving74\","
data+="\"email\":\"flag{ENC0D1NG_D4TA_1S_N0T_ENCRY7I0N}\"" # FLAG
data+=",\"address\":{\"street\":\"Cathryn Summit\",\"suite\":\"Suite 839\",\"city\":\"North Ellaton\",\"zipcode\":\"28286\",\"geo\":{\"lat\":\"53.2987\",\"lng\":\"-170.7903\"}},\"phone\":\"1-748-129-4308 x11404\",\"website\":\"laisha.org\",\"company\":{\"name\":\"Cremin, Kovacek and Blick\",\"catchPhrase\":\"Object-based asynchronous data-warehouse\",\"bs\":\"viral benchmark networks\"}}"

# Generate final 43 records
for x in `seq 58  100`; 
do 
  echo -n ".";
  data+=",\"user$x\":"; 
  data+=$(faker-cli --helpers userCard);
done;

data+="}";

echo ""
echo "dummping data to users.json";
echo $data > $filename


# ************************* base64 encode file *************************
echo "encode file to base64"
$(base64 users.json -o users.b64)


# ************************* convert fie to hex output *************************
echo "encode base64 file to hex"
$(xxd -ps users.b64 > users_db)


# ************************* cleanup *************************
$(rm users.json)
$(rm users.b64)

echo "done!"

