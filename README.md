# Binary 1

#### Description
A user accidentally installed malware on their computer and now the user database is unavailable. Can you recover the data and the flag?

#### Flag
flag{ENC0D1NG_D4TA_1S_N0T_ENCRY7I0N}

#### Hints
I don't think the malware engineers were very good at crytography

#### Solution
xxd -r -ps users_db | base64 -D | grep -E -o '.flag.{0,33}'
